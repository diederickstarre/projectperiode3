<!DOCTYPE html>
<html>
<head>
	<title>Foto's</title>
	<meta charset="UTF-8">
	<link rel="stylesheet" type="text/css" href="css/mystyles.css?p=<?php echo time();?>">
        <link rel="stylesheet" type="text/css" href="css/font-awesome.css">
</head>
<body>
<div class="main">
<div class="bovenstelaag"> 
    <center class="telefoonnummer"> Telefoon: 035-1234567 </center>
        <div class="afbeeldingenbovenstelaag">
            <a href="https://gmail.com/"><img class="icooneninzwartebalk" src="images/emailimage.png" href="">          
            <a href="https://facebook.com/"><img class="icooneninzwartebalk" src="images/facebookimage.png" href="">
            <a href="https://twitter.com/"><img class="icooneninzwartebalk" src="images/twitterimage.png" >
            <a href="https://wifi.com/"><img class="icooneninzwartebalk" src="images/wifiimage.png" href="">
        </div>
</div>
<div class="logo">
    <header> <img class ="logo" src="images/logo-CharlesGabriel.png" >
</div>
<div class="menubar">
    <ul>
        <li><a class="menu" href="CharlesGabrielHomepage.php">Home <i class="fa fa-angle-down" style="color: #93b2cc" aria-hidden="true"></i> </a></li>
        <li><a class="menu" href="CharlesGabrielprojecten.php">Projecten <i class="fa fa-angle-down" style="color: #93b2cc" aria-hidden="true"></i> </a></li>
        <li><a class="menu" href="CharlesGabrielFotos.php">Foto's <i class="fa fa-angle-down" style="color: #93b2cc" aria-hidden="true"></i></a></li>
        <li><a class="menu" href="CharlesGabrielOverons.php">Over ons <i class="fa fa-angle-down" style="color: #93b2cc" aria-hidden="true"></i></a></li>
        <li><a class="menu" href="CharlesGabrielContact.php">Contact <i class="fa fa-angle-down" style="color: #93b2cc" aria-hidden="true"></i> </a></li>
    </ul>
</div> 
<div class="blauwebalk">
</div>
<div class="afbeeldingenprojecten">
    <img class="afbeeldingenproject" src="images/project1.jpg">
    <img class="afbeeldingenproject" src="images/project2.jpg">
    <img class="afbeeldingenproject" src="images/project3.jpg">
    <img class="afbeeldingenproject" src="images/project4.jpg">
    <br><input class= "fotoshoutwerkenknop" type= "button" value="Houtbouw >"></br>
    <img class="tweederijafbeeldingenproject" src="images/project5.jpg">
    <img class="tweederijafbeeldingenproject" src="images/project6.jpg">
    <img class="tweederijafbeeldingenproject" src="images/project7.jpg">
    <img class="tweederijafbeeldingenproject" src="images/project8.jpg">
    <br><input class= "fotoshoutwerkenknop" type= "button" value="verbouwing >"></br>
</div>
<div class="grijzebalkonderaanprojecten">
</div>     
 <div class="catalogus">
     <div class= "Vindonsop">
         <h2>Vind ons op</h2>
         <p>-facebook.com/charlesgabriel</p>
         <p>-twitter.com/charlesgabriel</P>
         <p>Industrieweg 10</p>
         <P>3762 EK SOEST</p>
         <p>035-88 73 471<p>
         <p>06-53 90 69 20</p>
         <p>info@charlesgabriel.nl</p>
    </div>
    <div class="Overons">
        <h2> Over ons</h2>
        <p>Lorem ipsum dolor sit amet,</p> <p>consectetur adipiscing elit, sed do </p>eiusmod tempor 
           incididunt ut labore </p>et dolore magna aliqua.Ut enim ad </p>minimveniamquisnostrud 
           exercitation</p> ullamco laboris nisi ut aliquip ex.</p>
    </div>
    <div class="Snelmenu">
        <h2> Snelmenu </h2>
        <p> > Lorem ipsum dolor sit amet </p>
        <p> > consectetur adipiscing elit </p>
        <p> > mod tempor incididunt ut labore </p>
        <p> > et dolore magna aliqu </p>
        <p> > Ut enim ad minim veniam</p>
    </div>
    <div class="fotogalerij">
        <h2>fotogallerij</h2>
        <img src="images/fotosfotogalerij.PNG">
    </div>
</div>
<div class="Onderstelaag"> 
        <div class="afbeeldingenonderstelaag">
          <a href="https://gmail.com/"><img class="icooneninzwartebalk" src="images/emailimage.png" href="">          
            <a href="https://facebook.com/"><img class="icooneninzwartebalk" src="images/facebookimage.png" href="">
            <a href="https://twitter.com/"><img class="icooneninzwartebalk" src="images/twitterimage.png" >
            <a href="https://wifi.com/"><img class="icooneninzwartebalk" src="images/wifiimage.png" href="">
        </div>
</div>

</div>
</body>
</html>