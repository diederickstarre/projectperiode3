
<!DOCTYPE html>
<html>
<head>
	<title>Home pagina</title>
	<meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" type="text/css" href="css/mystyles.css?p=<?php echo time();?>">
    <link rel="stylesheet" type="text/css" href="css/font-awesome.css">
</head>
<body>
<div class="main">
<div class="bovenstelaag"> 
    <center class="telefoonnummer"> Telefoon: 035-1234567 </center>
        <div class="afbeeldingenbovenstelaag">
            <a href="https://gmail.com/"><img class="icooneninzwartebalk" src="images/emailimage.png" href=""></a>          
            <a href="https://facebook.com/"><img class="icooneninzwartebalk" src="images/facebookimage.png" href=""></a>
            <a href="https://twitter.com/"><img class="icooneninzwartebalk" src="images/twitterimage.png" ></a>
            <a href="https://wifi.com/"><img class="icooneninzwartebalk" src="images/wifiimage.png" href=""></a>
        </div>
</div>
<div class="logo">
    <header> <img class ="logo" src="images/logo-CharlesGabriel.png" >
</div>
<div class="menubar">
    <ul>
        <li><a class="menu" href="CharlesGabrielHomepage.php">Home<i class="fa fa-angle-down" style="color: #93b2cc" aria-hidden="true"></i></a></li>
        <li><a class="menu" href="CharlesGabrielprojecten.php">Projecten<i class="fa fa-angle-down" style="color: #93b2cc" aria-hidden="true"></i> </a></li>
        <li><a class="menu" href="CharlesGabrielFotos.php">Foto's<i class="fa fa-angle-down" style="color: #93b2cc" aria-hidden="true"></i> </a></li>
        <li><a class="menu" href="CharlesGabrielOverons.php">Over ons<i class="fa fa-angle-down" style="color: #93b2cc" aria-hidden="true"></i> </a></li>
        <li><a class="menu" href="CharlesGabrielContact.php">Contact <i class="fa fa-angle-down" style="color: #93b2cc" aria-hidden="true"></i> </a></li>
    </ul>
</div> 
<div class="slideshow">
<h2 class="eersteregel">HET COMPLETE TIMMER- EN</h2>
<h2 class="tweederegel">MONTAGEBEDRIJF DAT WERKT VOOR ZOWEL</h2>
<h2 class="derderegel">PARTICULIEREN ALS BEDRIJVEN </h2>
<div class="slideshow2" style="width:1440px">
  <img class="mySlides" src="images/slideshow.png">
  <img class="mySlides" src="images/afbeeldingbouw.jpg">
  
</div>

<script>
var myIndex = 0;
carousel();

function carousel() {
    var i;
    var x = document.getElementsByClassName("mySlides");
    for (i = 0; i < x.length; i++) {
       x[i].style.display = "none";  
    }
    myIndex++;
    if (myIndex > x.length) {myIndex = 1}    
    x[myIndex-1].style.display = "block";  
    setTimeout(carousel, 2000); // Change image every 4 seconds
}
</script>

 
</div>



<div class="icoononderslideshow">
    <div class="test" ><img  src="images/icoon-nieuwbouw.png">
    <h2>Nieuwbouw</h2>
    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor.</p>
    </div>
    <div class="test" ><img   src="images/icoon-verbouw.png">
    <h2>Verbouw</h2>
    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor.</p>
    </div>
    <div class="test" ><img   src="images/icoon-timmerwerk.png">
    <h2>Timmerwerk</h2>
    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor.</p>
    </div>
    <div class="test" ><img   src="images/icoon-afbouw.png">
    <h2>Afbouw</h2>
    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor.</p>
    </div>
</div>
<div class="textineenbalk">
    <h2>
        <center> <font>Lorem ipsum dolor sit amet, consectetur adipiscing elit,
             sed do </font><font color="ghostwhite;">eiusmod tempor</font>
        </center>
    </h2>
</div>
<div class="overservice">
    <div class="texten">
        <h2>Wie we zijn</h2>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor 
            incididunt ut labore et dolore magna liqua. Ut enim ad minim veniam, quis nostrud 
            exercitation ullamco.</p>
    </div>
    <div class="texten">
        <h2>Waarom bij ons</h2>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor 
            incididunt ut labore et dolore magna liqua. Ut enim ad minim veniam, quis nostrud 
            exercitation ullamco.</p>
    </div>
    <div class="texten">
        <h2>Professionele service</h2>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor 
            incididunt ut labore et dolore magna liqua. Ut enim ad minim veniam, quis nostrud 
            exercitation ullamco.</p>
    </div>
    <div class="fotobouwplaats">
        <img class="fotobouwplaatsch" src="images/bouwplaats.png">
    </div>
</div>
<div class="onderstefoto">
    <img src="images/fotovankamer.png" class="bouwplaats">
<h2 class="eersteregelfotovankamer"><center>Sed ut perspiciatis unde omnis iste natus error sit voluptatem ac</center></h2>
<h2 class="tweederegelfotovankamer"><center>cusantium doloremque laudantium, totam rem aperiam, eaque ipsa</center> </h2>
<h2 class="derderegelfotovankamer"><center>quae ab illo inventore veritatis</center> </h2>
</div>
<div class="vraagmetknop">
    <h2><center> Excepteur sint occaecat cupidatat non proident, sunt in culpa qui?  &nbsp &nbsp &nbsp &nbsp &nbsp<input class= "Offerteknop" type= "button" value="Offerte"><input class= "Informatieknop" type= "button" value="informatie"></center></h2>
 </div>
 <div class="catalogus">
     <div class= "Vindonsop">
         <h2>Vind ons op</h2>
         <p>-facebook.com/charlesgabriel</p>
         <p>-twitter.com/charlesgabriel</P>
         <p>Industrieweg 10</p>
         <P>3762 EK SOEST</p>
         <p>035-88 73 471<p>
         <p>06-53 90 69 20</p>
         <p>info@charlesgabriel.nl</p>
    </div>
    <div class="Overons">
        <h2> Over ons</h2>
        <p>Lorem ipsum dolor sit amet,</p> <p>consectetur adipiscing elit, sed do </p>eiusmod tempor 
           incididunt ut labore </p>et dolore magna aliqua.Ut enim ad </p>minimveniamquisnostrud 
           exercitation</p> ullamco laboris nisi ut aliquip ex.</p>
    </div>
    <div class="Snelmenu">
        <h2> Snelmenu </h2>
        <p> > Lorem ipsum dolor sit amet </p>
        <p> > consectetur adipiscing elit </p>
        <p> > mod tempor incididunt ut labore </p>
        <p> > et dolore magna aliqu </p>
        <p> > Ut enim ad minim veniam</p>
    </div>
    <div class="fotogalerij">
        <h2>fotogallerij</h2>
        <img src="images/fotosfotogalerij.PNG">
    </div>
</div>
<div class="Onderstelaag"> 
        <div class="afbeeldingenonderstelaag">
            <a href="https://gmail.com/"><img class="icooneninzwartebalk" src="images/emailimage.png" href="">          
            <a href="https://facebook.com/"><img class="icooneninzwartebalk" src="images/facebookimage.png" href="">
            <a href="https://twitter.com/"><img class="icooneninzwartebalk" src="images/twitterimage.png" >
            <a href="https://wifi.com/"><img class="icooneninzwartebalk" src="images/wifiimage.png" href="">
        </div>
        
</div>
</div>
</body>
</html>