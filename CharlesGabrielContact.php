<!DOCTYPE html>
<html>
<head>
	<title>Contact</title>
	<meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">        
    <link rel="stylesheet" type="text/css" href="css/mystyles.css?p=<?php echo time();?>">
    <link rel="stylesheet" type="text/css" href="css/font-awesome.css">
</head>
<body>
<div class="main">
<div class="bovenstelaag"> 
    <center class="telefoonnummer"> Telefoon: 035-1234567 </center>
        <div class="afbeeldingenbovenstelaag">
            <a href="https://gmail.com/"><img class="icooneninzwartebalk" src="images/emailimage.png" href="">          
            <a href="https://facebook.com/"><img class="icooneninzwartebalk" src="images/facebookimage.png" href="">
            <a href="https://twitter.com/"><img class="icooneninzwartebalk" src="images/twitterimage.png" >
            <a href="https://wifi.com/"><img class="icooneninzwartebalk" src="images/wifiimage.png" href="">
        </div>
</div>
<div class="logo">
    <header> <img class ="logo" src="images/logo-CharlesGabriel.png" >
</div>
<div class="menubar">
    <ul>
    <li><a class="menu" href="CharlesGabrielHomepage.php">Home <i class="fa fa-angle-down" aria-hidden="true"></i> </a></li>
        <li><a class="menu" href="CharlesGabrielprojecten.php">Projecten <i class="fa fa-angle-down" style="color: #93b2cc" aria-hidden="true"></i> </a></li>
        <li><a class="menu" href="CharlesGabrielFotos.php">Foto's <i class="fa fa-angle-down" style="color: #93b2cc" aria-hidden="true"></i></a></li>
        <li><a class="menu" href="CharlesGabrielOverons.php">Over ons <i class="fa fa-angle-down" style="color: #93b2cc" aria-hidden="true"></i></a></li>
        <li><a class="menu" href="CharlesGabrielContact.php">Contact <i class="fa fa-angle-down" style="color: #93b2cc" aria-hidden="true"></i></a></li>
    </ul>
</div> 
<div class="blauwebalk">
</div>
<h1 class="kopjeContact">Contact</h1>
 <div class="invoerenenmaps">
    <div id="map">
        <script>
        function initMap() {
            var uluru = {lat: 52.180364, lng: 5.273146};
            var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 10,
            center: uluru
            });
            var marker = new google.maps.Marker({
            position: uluru,
            map: map
            });
        }
            </script>
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDqeS7fDhOWPU7r4iNwg4dYRSUHD-jEKa0&callback=initMap">
        async defer></script>
    </div>

    <div class="invoerengegevens">
    Je naam:(verplicht)<br>
    <input  class= "invoervelden" type="text" name="firstname" value="Invoeren"><br>
    Je e-mail adress:(verplicht)<br>
    <input class= "invoervelden"  type="text" name="firstname" value="Invoeren"><br>
    Je onderwerp:(verplicht)<br>
    <input class= "invoervelden"  type="text" name="firstname" value="Invoeren"><br>
    Je telefoonnummer:(verplicht)<br>
    <input class= "invoervelden" type="text" name="firstname" value="Invoeren"><br>
    Je bericht:(verplicht)<br>
    <textarea class= "grootsteinvoerveld"  type="text" name="firstname" value="Invoeren"></textarea><br>
    <input class= "contactsubmitknop" type= "submit" name="sumbitknop" value="invoeren">
    </div> 
</div>


<div class="grijzebalkonderaanprojecten">
</div>     
 <div class="catalogus item" >
     <div class= "Vindonsop item">
         <h2>Vind ons op</h2>
         <p>-facebook.com/charlesgabriel</p>
         <p>-twitter.com/charlesgabriel</P>
         <p>Industrieweg 10</p>
         <P>3762 EK SOEST</p>
         <p>035-88 73 471<p>
         <p>06-53 90 69 20</p>
         <p>info@charlesgabriel.nl</p>
    </div>
    <div class="Overons item">
        <h2> Over ons</h2>
        <p>Lorem ipsum dolor sit amet,</p> <p>consectetur adipiscing elit, sed do </p>eiusmod tempor 
           incididunt ut labore </p>et dolore magna aliqua.Ut enim ad </p>minimveniamquisnostrud 
           exercitation</p> ullamco laboris nisi ut aliquip ex.</p>
    </div>
    <div class="Snelmenu item">
        <h2> Snelmenu </h2>
        <p> > Lorem ipsum dolor sit amet </p>
        <p> > consectetur adipiscing elit </p>
        <p> > mod tempor incididunt ut labore </p>
        <p> > et dolore magna aliqu </p>
        <p> > Ut enim ad minim veniam</p>
    </div>
    <div class="fotogalerij item">
        <h2>fotogallerij</h2>
        <img src="images/fotosfotogalerij.PNG">
    </div>
</div>
<div class="Onderstelaag"> 
        <div class="afbeeldingenonderstelaag">
            <a href="https://gmail.com/"><img class="icooneninzwartebalk" src="images/emailimage.png">          
            <a href="https://facebook.com/"><img class="icooneninzwartebalk" src="images/facebookimage.png">
            <a href="https://twitter.com/"><img class="icooneninzwartebalk" src="images/twitterimage.png" >
            <a href="https://wifi.com/"><img class="icooneninzwartebalk" src="images/wifiimage.png" >
        </div>
</div>

</div>
</body>
</html>