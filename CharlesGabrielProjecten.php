<!DOCTYPE html>
<html>
<head>
	<title>Projecten</title>
	<meta charset="UTF-8">
	<link rel="stylesheet" type="text/css" href="css/mystyles.css?p=<?php echo time();?>">
        <link rel="stylesheet" type="text/css" href="css/font-awesome.css">
</head>
<body>
<div class="main">
<div class="bovenstelaag"> 
    <center class="telefoonnummer"> Telefoon: 035-1234567 </center>
        <div class="afbeeldingenbovenstelaag">
           <a href="https://gmail.com/"><img class="icooneninzwartebalk" src="images/emailimage.png" href="">          
            <a href="https://facebook.com/"><img class="icooneninzwartebalk" src="images/facebookimage.png" href="">
            <a href="https://twitter.com/"><img class="icooneninzwartebalk" src="images/twitterimage.png" >
            <a href="https://wifi.com/"><img class="icooneninzwartebalk" src="images/wifiimage.png" href="">
        </div>
</div>
<div class="logo">
    <header> <img class ="logo" src="images/logo-CharlesGabriel.png" >
</div>
<div class="menubar">
    <ul>
        <li><a class="menu" href="CharlesGabrielHomepage.php">Home<i class="fa fa-angle-down" style="color: #93b2cc" aria-hidden="true"></i></a></li>
        <li><a class="menu" href="CharlesGabrielprojecten.php">Projecten<i class="fa fa-angle-down" style="color: #93b2cc" aria-hidden="true"></i> </a></li>
        <li><a class="menu" href="CharlesGabrielFotos.php">Foto's<i class="fa fa-angle-down" style="color: #93b2cc" aria-hidden="true"></i> </a></li>
        <li><a class="menu" href="CharlesGabrielOverons.php">Over ons<i class="fa fa-angle-down" style="color: #93b2cc" aria-hidden="true"></i> </a></li>
        <li><a class="menu" href="CharlesGabrielContact.php">Contact <i class="fa fa-angle-down" style="color: #93b2cc" aria-hidden="true"></i> </a></li>
    </ul>
</div> 

<div class="blauwebalk">
</div>  

<h2 class="verbouwingsnaam"> Verbouwing Rietweg 33</h2>
<h2 class="kortetekstkop"> KORTE TEKST </h2>
<div class="afbeeldingeninprojecten">
    <img class="afbeeldingenverbouwing" src="images/project1.jpg">
    <img class="afbeeldingenverbouwing" src="images/project1.jpg">
    <img class="afbeeldingenverbouwing" src="images/project1.jpg">
    <img class="afbeeldingenverbouwing" src="images/project1.jpg">
    <img class="afbeeldingenverbouwing" src="images/project1.jpg">
    <img class="afbeeldingenverbouwing" src="images/project1.jpg">
    <img class="afbeeldingenverbouwing" src="images/project1.jpg">
    <img class="afbeeldingenverbouwing" src="images/project1.jpg">
    <img class="afbeeldingenverbouwing" src="images/project1.jpg">
    <img class="afbeeldingenverbouwing" src="images/project1.jpg">
    <img class="afbeeldingenverbouwing" src="images/project1.jpg">
    <img class="afbeeldingenverbouwing" src="images/project1.jpg">
    <img class="afbeeldingenverbouwing" src="images/project1.jpg">
    <img class="afbeeldingenverbouwing" src="images/project1.jpg">
    <img class="afbeeldingenverbouwing" src="images/project1.jpg">
    <img class="afbeeldingenverbouwing" src="images/project1.jpg">
</div>
<div class="textoververbouwing"> 
<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. 
Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis,
ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, 
fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis 
vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus 
 elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat 
vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus 
viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. 
abitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing 
sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar,
 hendrerit id, lorem. Maecenas nec odio et 
 hendrerit id, lorem. Maecenas nec odio et 
 hendrerit id, lorem. Maecenas nec odio et 
 hendrerit id, lorem. Maecenas nec odio et 
 
 </p>
</div>

<div class="categorien">
    <h2 class="categories">Categoriën:<h2>
    <p> - Nieuwbouw<p>
    <p> - Verbouwing<p>
</div> 
<div class="grijzebalkonderaanprojecten">
</div>     
 <div class="catalogus">
     <div class= "Vindonsop">
         <h2>Vind ons op</h2>
         <p>-facebook.com/charlesgabriel</p>
         <p>-twitter.com/charlesgabriel</P>
         <p>Industrieweg 10</p>
         <P>3762 EK SOEST</p>
         <p>035-88 73 471<p>
         <p>06-53 90 69 20</p>
         <p>info@charlesgabriel.nl</p>
    </div>
    <div class="Overons">
        <h2> Over ons</h2>
        <p>Lorem ipsum dolor sit amet,</p> <p>consectetur adipiscing elit, sed do </p>eiusmod tempor 
           incididunt ut labore </p>et dolore magna aliqua.Ut enim ad </p>minimveniamquisnostrud 
           exercitation</p> ullamco laboris nisi ut aliquip ex.</p>
    </div>
    <div class="Snelmenu">
        <h2> Snelmenu </h2>
        <p> > Lorem ipsum dolor sit amet </p>
        <p> > consectetur adipiscing elit </p>
        <p> > mod tempor incididunt ut labore </p>
        <p> > et dolore magna aliqu </p>
        <p> > Ut enim ad minim veniam</p>
    </div>
    <div class="fotogalerij">
        <h2>fotogallerij</h2>
        <img src="images/fotosfotogalerij.PNG">
    </div>
</div>
<div class="Onderstelaag"> 
        <div class="afbeeldingenonderstelaag">
            <a href="https://gmail.com/"><img class="icooneninzwartebalk" src="images/emailimage.png" href="">          
            <a href="https://facebook.com/"><img class="icooneninzwartebalk" src="images/facebookimage.png" href="">
            <a href="https://twitter.com/"><img class="icooneninzwartebalk" src="images/twitterimage.png" >
            <a href="https://wifi.com/"><img class="icooneninzwartebalk" src="images/wifiimage.png" href="">
        </div>
</div>
</div>

</div>
</body>
</html>