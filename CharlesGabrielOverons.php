<!DOCTYPE html>
<html>
<head>
	<title>Over ons</title>
	<meta charset="UTF-8">
	<link rel="stylesheet" type="text/css" href="css/mystyles.css?p=<?php echo time();?>">
    <link rel="stylesheet" type="text/css" href="css/font-awesome.css">
</head>
<body>
<div class="main">
<div class="bovenstelaag"> 
    <center class="telefoonnummer"> Telefoon: 035-1234567 </center>
        <div class="afbeeldingenbovenstelaag">
            <a href="https://gmail.com/"><img class="icooneninzwartebalk" src="images/emailimage.png" href="">          
            <a href="https://facebook.com/"><img class="icooneninzwartebalk" src="images/facebookimage.png" href="">
            <a href="https://twitter.com/"><img class="icooneninzwartebalk" src="images/twitterimage.png" >
            <a href="https://wifi.com/"><img class="icooneninzwartebalk" src="images/wifiimage.png" href="">
        </div>
</div>
<div class="logo">
    <header> <img class ="logo" src="images/logo-CharlesGabriel.png" >
</div>
<div class="menubar">
    <ul>
      <li><a class="menu" href="CharlesGabrielHomepage.php">Home <i class="fa fa-angle-down" style="color: #93b2cc" aria-hidden="true"></i> </a></li>
        <li><a class="menu" href="CharlesGabrielprojecten.php">Projecten <i class="fa fa-angle-down" style="color: #93b2cc" aria-hidden="true"></i> </a></li>
        <li><a class="menu" href="CharlesGabrielFotos.php">Foto's <i class="fa fa-angle-down" style="color: #93b2cc" aria-hidden="true"></i></a></li>
        <li><a class="menu"href="CharlesGabrielOverons.php">Over ons <i class="fa fa-angle-down" style="color: #93b2cc" aria-hidden="true"></i></a></li>
        <li><a class="menu"href="CharlesGabrielContact.php">Contact <i class="fa fa-angle-down" style="color: #93b2cc" aria-hidden="true"></i> </a></li>
    </ul>
</div> 
<div class="blauwebalk">
</div>

<div class="Overcharlesgabriel">
    <h1> Over Charles Gabriel </h1>
</div>
<div class="textovercharlesgabriel">
<p> 
Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum 
sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies
 nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, 
 aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam 
 dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean
  vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam 
  lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet.
  Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. 
  Nam eget dui.
</p>
<p>Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing 
sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et 
ante tincidunt tempus. Donec vitae sapien ut libero venenatis faucibus. Nullam quis ante. Etiam sit amet orci 
ros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh. Donec sodales sagittis magna. Sed cons
equat, leo eget bibendum sodales
</p>
<p>augue velit cursus nunc, quis gravida magna mi a libero. Fusce vulputate eleifend sapien. Vestibulum purus quam, scelerisque ut, mollis sed, nonummy id, metus. Nullam accumsan lorem in dui. Cras ultricies mi eu turpis hendrerit fringilla. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; In ac dui quis mi consectetuer lacinia. 
Nam pretium turpis et arcu. Duis arcu tortor, suscipit eget, imperdiet nec, imperdiet iaculis, ipsum. Sed 
aliquam ultrices mauris. Integer ante arcu, accumsan a, consectetuer eget, posuere ut, mauris. Praesent
</p>
<p>
adipiscing. Phasellus ullamcorper ipsum rutrum nunc. Nunc nonummy metus. Vestibulum volutpat pretium libero. 
Cras id dui. Aenean ut eros et nisl sagittis vestibulum. Nullam nulla eros, ultricies sit amet, nonummy id, 
imperdiet feugiat, pede. Sed lectus. Donec mollis hendrerit risus. Phasellus nec sem in justo pellentesque 
facilisis. Etiam imperdiet imperdiet</p>
</div>
<div class="grijzebalkonderaanprojecten">
</div>     
 <div class="catalogus">
     <div class= "Vindonsop">
         <h2>Vind ons op</h2>
         <p>-facebook.com/charlesgabriel</p>
         <p>-twitter.com/charlesgabriel</P>
         <p>Industrieweg 10</p>
         <P>3762 EK SOEST</p>
         <p>035-88 73 471<p>
         <p>06-53 90 69 20</p>
         <p>info@charlesgabriel.nl</p>
    </div>
    <div class="Overons">
        <h2> Over ons</h2>
        <p>Lorem ipsum dolor sit amet,</p> <p>consectetur adipiscing elit, sed do </p>eiusmod tempor 
           incididunt ut labore </p>et dolore magna aliqua.Ut enim ad </p>minimveniamquisnostrud 
           exercitation</p> ullamco laboris nisi ut aliquip ex.</p>
    </div>
    <div class="Snelmenu">
        <h2> Snelmenu </h2>
        <p> > Lorem ipsum dolor sit amet </p>
        <p> > consectetur adipiscing elit </p>
        <p> > mod tempor incididunt ut labore </p>
        <p> > et dolore magna aliqu </p>
        <p> > Ut enim ad minim veniam</p>
    </div>
    <div class="fotogalerij">
        <h2>fotogallerij</h2>
        <img src="images/fotosfotogalerij.PNG">
    </div>
</div>
<div class="Onderstelaag"> 
        <div class="afbeeldingenonderstelaag">
            <a href="https://gmail.com/"><img class="icooneninzwartebalk" src="images/emailimage.png" href="">          
            <a href="https://facebook.com/"><img class="icooneninzwartebalk" src="images/facebookimage.png" href="">
            <a href="https://twitter.com/"><img class="icooneninzwartebalk" src="images/twitterimage.png" >
            <a href="https://wifi.com/"><img class="icooneninzwartebalk" src="images/wifiimage.png" href="">
        </div>
</div>
</div>
</body>
</html>